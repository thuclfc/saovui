/*!
 * sao_vui
 * 
 * 
 * @author Thuclfc
 * @version 2.0.0
 * Copyright 2020. MIT licensed.
 */$('#popup-register').modal('show');
// Create new wheel object specifying the parameters at creation time.
let theWheel = new Winwheel({
    'numSegments': 12, // Specify number of segments.
    'outerRadius': 293, // Set outer radius so wheel fits inside the background.
    'drawMode': 'image', // drawMode must be set to image.
    'drawText': true, // Need to set this true if want code-drawn text on image wheels.
    'textFontSize': 0, // Set text options as desired.
    'rotationAngle': 15,
    'responsive': true,

    'segments': // Define segments.
    [{ 'text': '' +
            '<span class="icon icon-star">1</span><span>500</span>' }, { 'text': '<span class="icon icon-star">2</span><span>Thẻ cào 100k</span>' }, { 'text': '<span class="icon icon-star">1</span><span>200</span>' }, { 'text': '<span class="icon icon-star">3</span><span>Thẻ cào 200k</span>' }, { 'text': '<span class="icon icon-star">1</span><span>100</span>' }, { 'text': '<span class="icon icon-star">3</span><span>Thẻ cào 500k</span>' }, { 'text': 'Trượt' }, { 'text': '<span class="icon icon-star">1</span><span>Thẻ cào 10k</span>' }, { 'text': '<span class="icon icon-star">1</span><span>5.000</span>' }, { 'text': '<span class="icon icon-star">1</span><span>Thẻ cào 20k</span>' }, { 'text': '<span class="icon icon-star">1</span><span>1.000</span>' }, { 'text': '<span class="icon icon-star">2</span><span>Thẻ cào 50k</span>' }],

    'animation': // Specify the animation to use.
    {
        'type': 'spinToStop',
        'duration': 8, // Duration in seconds.
        'spins': 8, // Number of complete spins.
        'callbackFinished': alertPrize
    }
});

// Create new image object in memory.
let loadedImg = new Image();

// Create callback to execute once the image has finished loading.
loadedImg.onload = function () {
    theWheel.wheelImage = loadedImg; // Make wheelImage equal the loaded image object.
    theWheel.draw(); // Also call draw function to render the wheel.
};

// Set the image source, once complete this will trigger the onLoad callback (above).
loadedImg.src = "assets/images/landing2/vongquay2.png";

// Vars used by the code in this page to do power controls.
let wheelPower = 0;
let wheelSpinning = false;

// -------------------------------------------------------


// -------------------------------------------------------
// Click handler for spin button.
// -------------------------------------------------------
function startSpin() {
    resetWheel();
    // Ensure that spinning can't be clicked again while already running.
    if (wheelSpinning == false) {
        // Based on the power level selected adjust the number of spins for the wheel, the more times is has
        // to rotate with the duration of the animation the quicker the wheel spins.

        if (wheelPower == 1) {
            theWheel.animation.spins = 2;
        } else if (wheelPower == 2) {
            theWheel.animation.spins = 5;
        } else if (wheelPower == 3) {
            theWheel.animation.spins = 8;
        }

        // Disable the spin button so can't click again while wheel is spinning.

        $('.spin_button_disable').addClass('active');

        // Begin the spin animation by calling startAnimation on the wheel object.
        theWheel.startAnimation();

        // Set to true so that power can't be changed and spin button re-enabled during
        // the current animation. The user will have to reset before spinning again.
        wheelSpinning = true;
    }
    setTimeout(function () {
        $('.spin_button_disable').removeClass('active');
    }, 8000);
}

// -------------------------------------------------------
// Function for reset button.
// -------------------------------------------------------
function resetWheel() {
    theWheel.stopAnimation(false); // Stop the animation, false as param so does not call callback function.
    theWheel.rotationAngle = 0; // Re-set the wheel angle to 0 degrees.
    theWheel.draw(); // Call draw to render changes to the wheel.

    wheelSpinning = false; // Reset to false to power buttons and spin can be clicked again.
}

// -------------------------------------------------------
// Called when the spin animation has finished by the callback feature of the wheel because I specified callback in the parameters.
// note the indicated segment is passed in as a parmeter as 99% of the time you will want to know this to inform the user of their prize.
// -------------------------------------------------------
function alertPrize(indicatedSegment) {
    // Do basic alert of the segment text. You would probably want to do something more interesting with this information.
    $('.content_code').html(indicatedSegment.text);
    $('#popup-code').modal('show');
}